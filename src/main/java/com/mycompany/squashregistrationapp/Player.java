/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.squashregistrationapp;

import java.util.Scanner;

/**
 *
 * @author Kyle
 */
public class Player {
        
    private String name = "";
    private String parentName = "";
    private String phone = "";
    private String email = "";
    private double amountPaid = 0;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(double amountPaid) {
        this.amountPaid = amountPaid;
    }
    
    public Player(Scanner input){
        System.out.println("What is the player's name?");
        this.name = input.nextLine();
        System.out.println("What is the player's parent's name?");
        this.parentName=input.nextLine();
        System.out.println("What is the player's phone number?");
        this.phone = input.nextLine();
        System.out.println("What is the player's e-mail address?");
        this.email = input.nextLine();
        System.out.println("What is the amount paid for the player?");
        this.amountPaid = input.nextDouble();
        input.nextLine();
    }
    
    public Player(){
        
    }
    
    @Override
    public String toString(){
        String output = "Player Name: " + this.name ;
        output += "\nPlayer Parent's Name: " + this.parentName;
        output += "\nPlayer's E-mail Address: " + this.email;
        output += "\nPlayer's Phone Number: " + this.phone;
        output += "\nPlayer's amount Paid: " + this.amountPaid +"\n";
        return output;
    }
}
