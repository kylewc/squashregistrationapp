/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.squashregistrationapp;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kyle
 */
public class SquashRegistrationMain {
    
    public static final String PLAYERS_PATH = "c:\\cis2232\\players.txt";
    public static void main(String[] args) {
        
       
        ArrayList<Player> playersList = new ArrayList<>();
        try {
            playersList = doesFileExist(playersList);
        } catch (IOException ex) {
            Logger.getLogger(SquashRegistrationMain.class.getName()).log(Level.SEVERE, null, ex);
        }



        boolean finished = false;
        //Scanner for input from the user.
        Scanner input = new Scanner(System.in);

        //Prompt to be used for the menu.
        String prompt = "\nEnter menu choice:"
                + "\nA) Add Player"
                //Show all of the player details to the console.
                + "\nS) Show Players"
                //Allow the user to update the amount paid for any player in the ArrayList created above. 
                //After updating a pay amount show the new total amount paid by all players.
                + "\nG) Update amount paid."
                + "\nX) Exit";
        //String to hold user's choice
        String userChoice;
        //while the user is not finished, continue to prompt them with the menu.
        do {
            PrintWriter writer = null;
                System.out.println(prompt);
                userChoice = input.nextLine().toUpperCase();
                switch (userChoice) {
                    case "A":
                        playersList.add(new Player(input));
                        break;
                    case "S":
                        for (Player player : playersList){
                            System.out.println(player.toString());
                        }
                        break;
                    case "G":
                        System.out.println("Which player (name) would you like to update an amount for?");
                        for (Player player : playersList){
                            System.out.println(player.getName());
                        }
                        String selectedPlayer = input.nextLine().toLowerCase();
                        int i = 0;
                        int totalAmountPaid = 0;
                        for (Player player : playersList){
                            
                            if(player.getName().toLowerCase().equals(selectedPlayer)){
                                System.out.println("What is the new amount?");
                                
                                playersList.get(i).setAmountPaid(input.nextDouble());
                                input.nextLine();
                                
                            }
                            else{
                                
                                ++i;
                                if (i == playersList.size()){
                                    System.out.println("A player was nnot located by that name.");
                                }
                            }
                            totalAmountPaid += playersList.get(i).getAmountPaid();
                            
                        }
                        System.out.println("The new total amount paid for all players is:");
                        for (Player player : playersList){
                            System.out.println("Player Name: " + player.getName() + "\nPaid Amount: " + player.getAmountPaid());
                            
                        }
                        break;
                    case "X":
                        ObjectMapper mapper = new ObjectMapper();
                        {
                            try {
                                writer = new PrintWriter (PLAYERS_PATH);
                                mapper.writeValue(writer, playersList);
                                writer.close();

                            } catch (IOException ex) {
                                Logger.getLogger(SquashRegistrationMain.class.getName()).log(Level.SEVERE, null, ex);
                            }

                        }
                        finished = true;
                        break;
                    default:
                        System.out.println("Invalid choice!");
                        break;
                }
            
            
        } while (finished == false);
        System.exit(0);
    }

    public static ArrayList<Player> doesFileExist(ArrayList<Player> playersList) throws IOException{
                File file = new File(PLAYERS_PATH);
                if(file.exists()){
                ObjectMapper mapper = new ObjectMapper();
                TypeReference type = new TypeReference<ArrayList<Player>>(){};
                ArrayList<Player> tempList;
                    tempList = mapper.readValue(file, type);
                    playersList=tempList;
                if (tempList.isEmpty()){
                    playersList = tempList;
                    return playersList;
                }
            }
                
        else{
            File newFile = new File("c:\\cis2232\\");
            if(newFile.mkdirs()){
                System.err.println("Fle was not found, but directory has been created.");
            }
        }
        return playersList;
    }
}
